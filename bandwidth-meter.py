
from datetime import datetime
import os.path
import sys
import csv
from urllib.request import urlopen, Request
from urllib.error import URLError, HTTPError

def isfloat(value):
  try:
    float(value)
    return True
  except ValueError:
    return False

class BandwidthMeter:

  def _reset(self):
    self._check_report_creation = False
    self._begin_timestamp = datetime.now()
    self._csv_data_result =[]


  def __init__(self):
    self._reset()
    self._csv_data_filename_template = '{year}-{month:0>2}_data.csv'  
    self._csv_report_filename_template = '{year}_report.csv'
    self._csv_data_fieldnames = ['timestamp', 'filesize_bytes', 'duration_seconds','bandwidth_mbits','status_code','url'] 
    self._csv_report_fieldnames = ['year','month','bandwidth']


  def _download_url(self, url, chunk_size=1024*1024):
    #response = urlopen(url)
    headers = {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0'}
    try:
      response = urlopen(Request(url, headers=headers))
    except HTTPError as e:
      print('Error code: ', e.code)
      return {
        'code': e.code,
        'length': 0
      }
    except URLError as e:
      print('Reason: ', e.reason)
      return {
        'code': -1,
        'length': 0
      }
    else:
      while True:
        chunk = response.read(chunk_size)
        if not chunk:
          break
      return {
        'code': response.getcode(),
        'length': response.info().get('Content-Length')
      }



  def _download(self, url):
    # download file and measure time
    begin = datetime.now()
    r = self._download_url(url)
    end = datetime.now()

    # calculate report values
    duration = (end - begin).total_seconds()
    filesize = int(r.get('length'))
    bandwith = filesize * 8.0 / duration / 1000 / 1000 
    bandwidth_str = str(round(bandwith,2)) if filesize > 0 else '--'
    print('download of {} bytes took {} seconds => {} MBit/s'.format(filesize,duration,bandwidth_str))

    # CSV save temporary data entry
    self._csv_data_result.append({
      'timestamp'         : begin.isoformat(),
      'filesize_bytes'    : str(round(filesize,2)),
      'duration_seconds'  : duration,
      'bandwidth_mbits'   : bandwidth_str,
      'status_code'       : r.get('code'),
      'url'               : url
    })


  def _save_data(self):
    # prepare variables
    year = self._begin_timestamp.year
    month = self._begin_timestamp.month
    out_filename = self._csv_data_filename_template.format(year=year, month=month)
    out_file_exists = os.path.isfile(out_filename) 

    # check if file already there
    # TODO: Not sure is this has to be negated or not
    if not out_file_exists: 
      self._check_report_creation = True

    # write data csv file
    with open(out_filename, 'a', newline='') as csvfile:
      writer = csv.DictWriter(csvfile, fieldnames=self._csv_data_fieldnames, quotechar='"', quoting=csv.QUOTE_ALL)
      if not out_file_exists:
        writer.writeheader()
      for d in self._csv_data_result:
        writer.writerow(d)
    

  def _save_report(self):
    if self._check_report_creation:
      # find last month (and year) to report for
      curr_year = self._begin_timestamp.year
      curr_month = self._begin_timestamp.month
      report_year = curr_year if curr_month > 1 else curr_year - 1
      report_month = curr_month - 1 if curr_month > 1 else 12

      bandwidth_sum = 0.0
      bandwidth_sum_n = 0

      # read data csv file
      data_filename = self._csv_data_filename_template.format(year=report_year, month=report_month)
      with open(data_filename, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
          if isfloat(row['bandwidth_mbits']):
            bandwidth_sum_n = bandwidth_sum_n + 1
            bandwidth_sum = bandwidth_sum + float(row['bandwidth_mbits'])

      # calculate average bandwidth
      avg_bandwidth = round(bandwidth_sum / bandwidth_sum_n, 2)

      # create output csv file name and put headers if necessary
      report_filename = self._csv_report_filename_template.format(year=report_year)
      report_file_exists = os.path.isfile(report_filename)

      # add this report to csv report file
      with open(report_filename, 'a', newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=self._csv_report_fieldnames, quotechar='"', quoting=csv.QUOTE_ALL)
        if not report_file_exists:
          writer.writeheader()
        writer.writerow({
          'year'      : report_year,
          'month'     : report_month, 
          'bandwidth' : avg_bandwidth
        }) 


  def run(self, url_list):
    self._reset()

    # test bandwidth on all urls
    i = 0
    for url in url_list:
      i = i + 1
      print('downloading file {} from {}...'.format(i,url))
      self._download(url) 
      continue
    
    # save bandwidth result to file
    self._save_data() 

    self._save_report()


def read_urls(filename):
  urls_list = []

  # use arguments if existent
  if len(sys.argv) > 1:
    for arg in sys.argv[1:]:
      urls_list.append(arg)
    return urls_list

  # read from file as fallback 
  file_lines = []
  with open(filename, 'r') as f:
    file_lines = f.readlines()

  # check if lines are not empty or do not start with '#'
  for line in file_lines:
    if len(line) > 1 and not line.startswith('#'):
      url = line[:-1] if line.endswith('\n') else line
      urls_list.append(url)

  return urls_list


if __name__ == '__main__':
  # read config, etc
  url_list = read_urls('test-files-urls.txt')
  print('test bandwidth with {} urls'.format(len(url_list)))
  
  # run tests
  bm = BandwidthMeter()
  bm.run(url_list)
