# Bandwidth Metering

- measure bandwidth by given download urls

## Usage

- script url url url
- or: script reads from testfile.txt

## Installation

- just clone this repo
- $ pipenv install

## Purpose

- check providers guaranteed bandwidth 
- create proof of bandwidth avail

## Disclaimer

- created during the holidays, when baby was taking a nap
- use urls u are allowed to use
- i do not provide test urls 
  - google for testfile download 
  - or host own testfile on own server
